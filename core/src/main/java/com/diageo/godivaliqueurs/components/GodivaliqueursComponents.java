/*******************************************************************************
 * Copyright (c) 2015 Diageo.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Diageo.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.diageo.godivaliqueurs.components;

/**
 * Home for all the constants related to Product components.
 * 
 */
public class GodivaliqueursComponents {
	
	
	/** constant for content results grid component. */
	public static final String CONTENT_RESULTS_GRID = "/apps/godivaliqueurs/components/contentResultsGrid";
	
	/** constant for content related content list component. */
	public static final String RELATED_CONTENT_LIST = "/apps/godivaliqueurs/components/relatedContentList";
	
	/** anchor link component path. */
	public static final String ANCHOR_LINK = "/apps/godivaliqueurs/components/anchorLinkNavigation";
	
	/** social saring component path. */
	public static final String SOCIAL_SHARING = "/apps/godivaliqueurs/components/socialSharing";
	
	/** The Constant ARTICLE_LISTING. */
	public static final String ARTICLE_LISTING = "/apps/godivaliqueurs/components/articleListing";
	
	/** The Constant PRODUCT_LISTING. */
	public static final String PRODUCT_LISTING = "/apps/godivaliqueurs/components/productListing";
	
	/** constant for home story component. */
	public static final String HOME_STORY = "/apps/godivaliqueurs/components/homeStory";
	
	/** constant for featured product component. */
	public static final String FEATURED_PRODUCT = "/apps/godivaliqueurs/components/featuredProduct";
	
	/** constant for featured article component. */
	public static final String FEATURED_ARTICLE = "/apps/godivaliqueurs/components/featuredArticle";
	
	/** constant for featured product range component. */
	public static final String FEATURED_PRODUCT_RANGE = "/apps/godivaliqueurs/components/featuredProductRange";
	
	/** constant for category overview component. */
	public static final String CATEGORY_OVERVIEW = "/apps/godivaliqueurs/components/categoryOverview";
	
	/** constant for product overview component. */
	public static final String PRODUCT_OVERVIEW = "/apps/godivaliqueurs/components/productOverview";
	
	/** The Constant PAGE_DIVIDER. */
	public static final String PAGE_DIVIDER = "/apps/godivaliqueurs/components/pageDivider";
	
	/** constant for product overview component. */
	public static final String SITEMAP = "/apps/godivaliqueurs/components/sitemap";
	
	/** constant for hero component. */
	public static final String HERO = "/apps/godivaliqueurs/components/hero";
	
	/** The Constant PRODUCT_TAG. */
	public static final String PRODUCT_TAG = "/apps/godivaliqueurs/components/productTags";
	
	/** The Constant MULTIPLE_RELATED_PRODUCT_BASE. */
	public static final String MULTIPLE_RELATED_PRODUCT_BASE = "/apps/godivaliqueurs/components/multipleRelatedProductsBase";
	
	/** The Constant MULTIPLE_RELATED_PRODUCT_ARTICLE. */
	public static final String MULTIPLE_RELATED_PRODUCT_ARTICLE = "/apps/godivaliqueurs/components/multipleRelatedProductsArticle";
	
	/** The Constant MULTIPLE_RELATED_ARTICLES_BASE. */
	public static final String MULTIPLE_RELATED_ARTICLES_BASE = "/apps/godivaliqueurs/components/multipleRelatedArticlesBase";
	
	/** constant for body copy steps component. */
	public static final String BODY_COPY_STEPS = "/apps/godivaliqueurs/components/bodyCopySteps";
	
	/** The Constant FOOTER. */
	public static final String FOOTER = "/apps/godivaliqueurs/components/footer";
	
	/** The Constant FEATURED_QUOTE. */
	public static final String FEATURED_QUOTE = "/apps/godivaliqueurs/components/featuredQuote";
	
	/** The Constant HEROLAUNCHVIDEO. */
	public static final String HEROLAUNCHVIDEO = "/apps/godivaliqueurs/components/heroLaunchVideo";
	
	/** The Constant MULTIPLE_RELATED_PRODUCT_ALTERNATE. */
	public static final String MULTIPLE_RELATED_PRODUCT_ALTERNATE = "/apps/godivaliqueurs/components/multipleRelatedProductAlternate";
	
	/** The Constant BODY_COPY. */
	public static final String BODY_COPY = "/apps/godivaliqueurs/components/bodyCopy";
	
	/** The Constant FEATURE BODY_COPY. */
	public static final String FEATURE_BODY_COPY = "/apps/godivaliqueurs/components/featurePhoneBodyCopy";
	
	/** The Constant SEARCH_RESULTS. */
	public static final String SEARCH_RESULTS = "/apps/godivaliqueurs/components/searchListing";
	
	/** The Constant IMAGE_FILE_REFERENCE. */
	public static final String IMAGE_FILE_REFERENCE = "fileReference";
	
	/** The Constant EXTENSION. */
	public static final String EXTENSION = "extension";
	
	/** The Constant FILENAME. */
	public static final String FILENAME = "fileName";
	
	/** The Constant IMAGE_ALT_TEXT. */
	public static final String IMAGE_ALT_TEXT = "altImage";
	
	/** The Constant IMAGE_ALT_TEXT_PROPERTY. */
	public static final String IMAGE_ALT_TEXT_PROPERTY = "alt";
	
	/** The Constant IMAGE_TITLE_PROPERTY. */
	public static final String IMAGE_TITLE_PROPERTY = "title";
	
	/** The Constant JSON_NAME_SPACE. */
	public static final String JSON_NAME_SPACE = "jsonNameSpace";
	
	/** The Constant URL. */
	public static final String URL = "url";
	
	/** The Constant IMAGE_PATH. */
	public static final String IMAGE_PATH = "path";
	
	/** The Constant WHATS_NEW. */
	public static final String WHATS_NEW = "/apps/godivaliqueurs/components/whatsNew";
	
	/** The Constant BODY_COPY_HEALINE. */
	public static final String BODY_COPY_HEALINE = "/apps/godivaliqueurs/components/bodyCopyHeadline";
	
	/** The Constant BREADCRUMB. */
	public static final String BREADCRUMB = "/apps/godivaliqueurs/components/breadcrumb";
	
	/** The Constant CAPTION. */
	public static final String CAPTION = "caption";
	
	/** The Constant REVIEW component. */
	public static final String REVIEW = "/apps/godivaliqueurs/components/reviews";
	/** constant for olapic component. */
	public static final String OLAPIC = "/apps/godivaliqueurs/components/olapic";
	
	/** constant for RECIPE component. */
	public static final String RECIPE = "/apps/godivaliqueurs/components/recipe";

	/** constant for product component. */
	public static final String PRODUCT = "/apps/godivaliqueurs/components/product";
	
	/** constant for a sample component. */
	public static final String SAMPLE = "/apps/godivaliqueurs/components/samplecomponent";

	
	/**
	 * Instantiates a new diageo components.
	 */
	private GodivaliqueursComponents() {
	
	}
}
