<%@ include file="/apps/diea/commons/global.jsp" %>
<%@ include file="/apps/iea/commons/clientlibs.jsp"%>
<cq:setContentBundle />

    <c:set var="title" value="${currentPage.title}" />

<!-- Recipe Template Starts-->

	<main id="main" class="recipe-template">
	<div itemscope itemtype="http://schema.org/Recipe">
          <div class="container_upper">
 			<cq:include path="flexi_upper" resourceType="foundation/components/parsys" />
        </div>
     <!-- Recipe-content starts here-->
            <section class="recipe-component section" data-track-id="analytics_recipe"> 
                   <cq:include path="recipe_component" resourceType="godivaliqueurs/components/recipe" /> 
            </section>

          <div class="container_lower">
           <cq:include path="flexi_lower" resourceType="foundation/components/parsys" />
         </div>
     <!-- recipe-component ends here-->
		</div>
	</main>
