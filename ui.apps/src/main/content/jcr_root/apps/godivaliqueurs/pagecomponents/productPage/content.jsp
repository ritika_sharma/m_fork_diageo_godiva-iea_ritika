<%@ include file="/apps/diea/commons/global.jsp" %>
<%@ include file="/apps/iea/commons/clientlibs.jsp"%>
<cq:setContentBundle />
<meta itemprop="brand" content="Godivaliqueurs">
    <c:set var="title" value="${currentPage.title}" />
     <main id="main" class="product-details-temp">
        <div itemscope itemtype="http://schema.org/Product">
		<section class="clearfix">
        	<div class="container-fluid">
           		<div class="row">
    				<cq:include path="hero_image" resourceType="godivaliqueurs/components/adaptiveImage" />
    			</div>
    		</div>
    	</section>
    	<section class="product clearfix" data-track-id="analytics_product">
       		<div class="container">
        		<div class="row">
					<cq:include path="product" resourceType="godivaliqueurs/components/product" />
				</div>
			</div>
		</section>
		<section class="product-features clearfix">
          	<div class="container">
              	<div class="row">
              		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<cq:include path="awards" resourceType="godivaliqueurs/components/richTextImage" />
					</div>
				</div>
			</div>
    	</section>
    	<section class="product-features clearfix">
          	<div class="container">
              	<div class="row">
              		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
    					<cq:include path="tasting_notes" resourceType="godivaliqueurs/components/richTextImage" />
    				</div>
					<div class="col-xs-12 recipe-mobile"></div>
    			</div>
			</div>
    	</section>
    	<section class="custom-social-sharing clearfix" data-track-id="analytics_socialSharing">
        	<div class="container">
            	<div class="row">
                	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
    					<cq:include path="social_media_headline" resourceType="godivaliqueurs/components/richTextImage" />
    					<cq:include path="social_share" resourceType="godivaliqueurs/components/socialSharing" />
    					<cq:include path="social_media_desc" resourceType="godivaliqueurs/components/richTextImage" />
    				</div>
    			</div>
			</div>
    	</section>
    	<section class="related-content clearfix" data-track-id="analytics_product">
         	<div class="container">
             	<div class="row">
    				<cq:include path="related_content" resourceType="godivaliqueurs/components/relatedContentList" />
    			</div>
    		</div>
    	</section>
		</div>
    </main>
    
    <cq:include path="product_par" resourceType="foundation/components/parsys" />
   
    <div class="o-continue-arrow"></div>
	<c:set var="includeCategory" value="css" scope="request" />
    <cq:include path="includeLib"
                resourceType="iea/components/includeClientLib" />
<c:if test="${isAdaptive == 'false'}">
    <c:set var="includeCategory" value="js" scope="request" />
    <cq:include path="includeLib"
            resourceType="iea/components/includeClientLib" />
</c:if>